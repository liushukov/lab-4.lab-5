<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Student Performance</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Styles -->
    <style>
        body {
            padding-top: 50px;
            text-align: center;
        }
        h1 {
            margin-bottom: 30px;
        }
        form {
            max-width: 400px;
            margin: 0 auto;
        }
        label {
            display: block;
            margin-bottom: 5px;
            text-align: left;
        }
        input[type="text"],
        input[type="date"],
        input[type="number"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        input[type="submit"] {
            width: 100%;
            padding: 10px;
            border: none;
            border-radius: 4px;
            background-color: #007bff;
            color: white;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #0056b3;
        }
        .error {
            color: red;
            font-size: 12px;
            margin-top: 5px;
            text-align: left;
        }
    </style>
    <!-- JavaScript Validation -->
    <script>
        function validateForm() {
            var surname = document.getElementById("surname").value;
            var name = document.getElementById("name").value;
            var secondName = document.getElementById("secondName").value;
            var submitDate = document.getElementById("submitDate").value;
            var mark = document.getElementById("mark").value;

            var currentYear = new Date().getFullYear();

            if (surname.match(/\d+/)) {
                document.getElementById("surnameError").innerText = "Surname should not contain numbers.";
                return false;
            } else {
                document.getElementById("surnameError").innerText = "";
            }

            if (name.match(/\d+/)) {
                document.getElementById("nameError").innerText = "Name should not contain numbers.";
                return false;
            } else {
                document.getElementById("nameError").innerText = "";
            }

            if (secondName.match(/\d+/)) {
                document.getElementById("secondNameError").innerText = "Second Name should not contain numbers.";
                return false;
            } else {
                document.getElementById("secondNameError").innerText = "";
            }

            var selectedDate = new Date(submitDate);
            if (selectedDate.getFullYear() !== currentYear) {
                document.getElementById("submitDateError").innerText = "Submit Date should be in the current year.";
                return false;
            } else {
                document.getElementById("submitDateError").innerText = "";
            }

            if (mark < 0 || mark > 5) {
                document.getElementById("markError").innerText = "Mark should be between 0 and 5.";
                return false;
            } else {
                document.getElementById("markError").innerText = "";
            }

            return true;
        }
    </script>


</head>
<body>
    <div class="container">
        <h1>Add Student Performance</h1>
        <form action="add" method="post" onsubmit="return validateForm()">
        <label for="surname">Surname:</label>
        <input type="text" id="surname" name="surname" required>
        <span id="surnameError" class="error"></span><br>

        <label for="name">Name:</label>
        <input type="text" id="name" name="name" required>
        <span id="nameError" class="error"></span><br>

        <label for="secondName">Second Name:</label>
        <input type="text" id="secondName" name="secondName" required>
        <span id="secondNameError" class="error"></span><br>

        <label for="submitDate">Submit Date:</label>
        <input type="date" id="submitDate" name="submitDate" required>
        <span id="submitDateError" class="error"></span><br>

        <label for="mark">Mark:</label>
        <input type="number" id="mark" name="mark" step="0.01" required>
        <span id="markError" class="error"></span><br>

            <input type="submit" value="Add">
        </form>
    </div>
</body>
</html>
