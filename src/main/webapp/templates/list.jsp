<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Student Management Application</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Styles -->
    <style>
        body {
            padding-top: 50px;
            text-align: center;
        }
        h1 {
            margin-bottom: 30px;
        }
        .btn {
            margin: 5px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Student Management</h1>
        <h2>
            <a href="add" class="btn btn-primary">Add New Student</a>
        </h2>
        <c:choose>
            <c:when test="${empty students}">
                <p>No students found.</p>
            </c:when>
            <c:otherwise>
                <div class="table-responsive">
                    <table class="table">
                        <caption>List of Students</caption>
                        <thead>
                            <tr>
                                <th>Surname</th>
                                <th>Name</th>
                                <th>Second Name</th>
                                <th>Submit Date</th>
                                <th>Mark</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="student" items="${students}">
                                <tr>
                                    <td><c:out value="${student.surname}" /></td>
                                    <td><c:out value="${student.name}" /></td>
                                    <td><c:out value="${student.secondName}" /></td>
                                    <td><c:out value="${student.submitDate}" /></td>
                                    <td><c:out value="${student.mark}" /></td>
                                    <td>
                                        <a href="edit?id=<c:out value='${student.id}' />" class="btn btn-info">Edit</a>
                                        <a href="delete?id=<c:out value='${student.id}' />" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</body>
</html>
