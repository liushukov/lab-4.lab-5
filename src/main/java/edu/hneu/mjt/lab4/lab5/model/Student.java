package edu.hneu.mjt.lab4.lab5.model;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "secondName")
    private String secondName;

    @Column(name = "submitDate")
    @Temporal(TemporalType.DATE)
    private Date submitDate;
    @Column(name = "mark")
    private double mark;

    public Student(){}
    public Student(int id, String name, String surname, String secondName, Date submitDate, double mark) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.submitDate = submitDate;
        this.mark = mark;
    }

    public Student(String name, String surname, String secondName, Date submitDate, double mark) {
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.submitDate = submitDate;
        this.mark = mark;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", secondName='" + secondName + '\'' +
                ", submitDate=" + submitDate +
                ", mark=" + mark +
                '}';
    }
}
