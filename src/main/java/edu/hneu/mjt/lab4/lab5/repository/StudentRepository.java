package edu.hneu.mjt.lab4.lab5.repository;

import edu.hneu.mjt.lab4.lab5.model.Student;

import java.util.List;

public interface StudentRepository {

    void saveStudent(Student student);

    void updateStudent(Student student);

    void deleteStudent(int id);

    Student getStudent(int id);

    List<Student> getAllStudents();
}
