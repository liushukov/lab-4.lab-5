package edu.hneu.mjt.lab4.lab5.controller;
import edu.hneu.mjt.lab4.lab5.dao.StudentDAO;
import edu.hneu.mjt.lab4.lab5.model.Student;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/")
public class MainController extends HttpServlet {

    private StudentDAO studentDAO;

    public void init() {
        studentDAO = new StudentDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/":
                showHomePage(request, response);
                break;
            case "/add":
                showAddForm(request, response);
                break;
            case "/list":
                listStudents(request, response);
                break;
            case "/delete":
                deleteStudent(request, response);
                break;
            case "/edit":
                editStudent(request, response);
                break;
            default:
                showErrorPage(request, response);
                break;
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/add":
                addStudent(request, response);
                break;
            case "/update":
                updateStudent(request, response);
                break;
        }
    }

    private void showHomePage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/home.jsp").forward(request, response);
    }

    private void showAddForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/add.jsp").forward(request, response);
    }

    private void addStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String secondName = request.getParameter("secondName");
        String submitDateStr = request.getParameter("submitDate");
        double mark = Double.parseDouble(request.getParameter("mark"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date submitDate = null;
        try {
            submitDate = sdf.parse(submitDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Student student = new Student(name, surname, secondName, submitDate, mark);
        studentDAO.saveStudent(student);

        response.sendRedirect(request.getContextPath() + "/list");
    }
    private void listStudents(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Student> students = studentDAO.getAllStudents();
        request.setAttribute("students", students);
        request.getRequestDispatcher("/templates/list.jsp").forward(request, response);
    }

    private void deleteStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        studentDAO.deleteStudent(id);
        response.sendRedirect(request.getContextPath() + "/list");
    }

    private void editStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Student student = studentDAO.getStudent(id);
        request.setAttribute("student", student);
        request.getRequestDispatcher("/templates/edit.jsp").forward(request, response);
    }
    private void updateStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String secondName = request.getParameter("secondName");
        String submitDateStr = request.getParameter("submitDate");
        double mark = Double.parseDouble(request.getParameter("mark"));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date submitDate = null;
        try {
            submitDate = sdf.parse(submitDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Student student = new Student(id, name, surname, secondName, submitDate, mark);
        studentDAO.updateStudent(student);
        response.sendRedirect(request.getContextPath() + "/list");
    }

    private void showErrorPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/templates/error.jsp");
        dispatcher.forward(request, response);
    }

}
