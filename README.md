## Лабораторна робота 4-5

### Тема: "Servlet"

#### Виконав: Люшуков Кирило


##  Постановка завдання
![Скріншот1](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/task1.1.png?ref_type=heads)
![Скріншот2](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/task1.2.png?ref_type=heads)
![Скріншот3](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/task1.3.png?ref_type=heads)
![Скріншот4](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/task1.4.png?ref_type=heads)
![Скріншот5](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/task1.5.png?ref_type=heads)
---
![Скріншот6](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project1.png?ref_type=heads)
**структура проєкту**
---
![Скріншот7](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project2.1.png?ref_type=heads)
![Скріншот8](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project2.2.png?ref_type=heads)
**файл з залежностями**
---
![Скріншот9](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project3.1.png?ref_type=heads)
![Скріншот10](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project3.2.png?ref_type=heads)
![Скріншот11](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project3.3.png?ref_type=heads)
![Скріншот12](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project3.4.png?ref_type=heads)
**MainController**
---
![Скріншот13](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project4.1.png?ref_type=heads)
![Скріншот14](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project4.2.png?ref_type=heads)
![Скріншот15](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project4.3.png?ref_type=heads)
**Student model**
---
![Скріншот16](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project5.png?ref_type=heads)
**Student repository**
---
![Скріншот17](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project6.1.png?ref_type=heads)
![Скріншот18](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project6.2.png?ref_type=heads)
![Скріншот19](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project6.3.png?ref_type=heads)
![Скріншот20](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project6.4.png?ref_type=heads)
![Скріншот21](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project6.5.png?ref_type=heads)
**StudentDAO**
---
![Скріншот22](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project7.1.png?ref_type=heads)
![Скріншот23](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project7.2.png?ref_type=heads)
**HibernateUtil та файл конфігурацій**
---
![Скріншот24](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project8.1.png?ref_type=heads)
![Скріншот25](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project8.2.png?ref_type=heads)
![Скріншот26](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project8.3.png?ref_type=heads)
![Скріншот27](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project8.4.png?ref_type=heads)
**використання jstl для forEach та клієнтська валідація**
---
![Скріншот28](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.1.png?ref_type=heads)
**головна сторінка**
![Скріншот29](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.2.png?ref_type=heads)
**сторінка зі студентами**
![Скріншот30](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.3.png?ref_type=heads)
**сторінка з редагуванням студента**
![Скріншот31](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.4.png?ref_type=heads)
**сторінка з додаванням студента**
![Скріншот32](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.5.png?ref_type=heads)
**успішно доданий студент**
![Скріншот33](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.6.png?ref_type=heads)
**демонстрація в таблиці бд**
![Скріншот34](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.7.png?ref_type=heads)
**видалення студента та демонстрація таблиці бд**
![Скріншот35](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.8.png?ref_type=heads)
**валідація прізвища**
![Скріншот36](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.9.png?ref_type=heads)
**валідація імені**
![Скріншот37](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.10.png?ref_type=heads)
**валідація побатькові**
![Скріншот38](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.11.png?ref_type=heads)
**валідація дати**
![Скріншот39](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.12.png?ref_type=heads)
**валідація оцінки**
![Скріншот40](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project9.13.png?ref_type=heads)
**обробка 404**
---
![Скріншот41](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project10.1.png?ref_type=heads)
**лог хібернейта**
![Скріншот42](https://gitlab.com/liushukov/lab-4.lab-5/-/raw/main/images/Project10.2.png?ref_type=heads)
**лог запитів**
---